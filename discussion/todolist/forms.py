from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label = "username", max_length = 20)
	password = forms.CharField(label = "password", max_length = 20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label = 'task_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label = "task_name", max_length = 50 )
	description = forms.CharField(label = "description", max_length = 200)
	status =  forms.CharField(label = "status", max_length =50)

class RegisterForm(forms.Form):
    username = forms.CharField(label='Username', max_length=20)
    firstname = forms.CharField(label='First Name', max_length=50)
    lastname = forms.CharField(label='Last Name', max_length=50)
    email = forms.EmailField(label='Email', max_length=50)
    password = forms.CharField(label='Password', max_length=50, widget=forms.PasswordInput)
    confirmPassword = forms.CharField(label='Confirm Password', max_length=50, widget=forms.PasswordInput)
    
class AddEventForm(forms.Form):
    event_name = forms.CharField(label = 'event_name', max_length = 50)
    description = forms.CharField(label = 'description', max_length = 200)
    
class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label = "event_name", max_length = 50 )
	description = forms.CharField(label = "description", max_length = 200)
	status =  forms.CharField(label = "status", max_length =50)

class UpdateProfileForm(forms.Form):
	firstname = forms.CharField(label = "firstname", max_length = 50)
	lastname = forms.CharField(label = "lastname", max_length = 50)
	password =  forms.CharField(label = "Password", max_length =50)
