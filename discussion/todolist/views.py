from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

# Local imports
from .models import ToDoItem, EventItem

# import the built in User Model
from django.contrib.auth.models import User

# to use the template created:
# from django.template import loader

# import the authenticate function
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash


from django.forms.models import model_to_dict

from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateEventForm, UpdateProfileForm

from django.utils import timezone

def index(request):
	eventitem_list = EventItem.objects.filter(user_id = request.user.id)
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	# template = loader.get_template("index.html")
	context = {
		'todoitem_list': todoitem_list,
		'eventitem_list': eventitem_list,
		"user": request.user
	}
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
	# render(request, route_template, context)
	return render(request, "index.html", context)

def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))

def eventitem(request, eventitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	eventitem = get_object_or_404(EventItem, pk = eventitem_id)

	return render(request, "eventitem.html", model_to_dict(eventitem))



# this function is responsible for registering on our application
def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['firstname']
            last_name = form.cleaned_data['lastname']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirmPassword']

            if password != confirm_password:
                context = {
                    'error': True,
                    'form': form,
                }
                return render(request, 'register.html', context)

            else:
                # check if a user with the same username already exists
                if User.objects.filter(username=username).exists():
                    context = {
                        'form': form,
                        'error1': True,
                    }
                    return render(request, 'register.html', context)

                # check if a user with the same email already exists
                if User.objects.filter(email=email).exists():
                    context = {
                        'form': form,
                        'error2': True,
                    }
                    return render(request, 'register.html', context)

                # create a new user with the submitted data
                user = User.objects.create_user(username=username, email=email,
                                                password=password, first_name=first_name,
                                                last_name=last_name)
                # redirect to a success page or login page
                return redirect("todolist:login")

    else:
        form = RegisterForm()

    context = {
        'form': form,
    }
    return render(request, 'register.html', context)

# def change_password(request):

# 	is_user_authenticated = False

# 	user = authenticate(username = "johndoe", password = "johndoe123456")

# 	if user is not None:
# 		authenticated_user = User.objects.get(username = 'johndoe')
# 		authenticated_user.set_password("johndoe1234567")
# 		authenticated_user.save()

# 		is_user_authenticated = True

# 	context = {
# 		"is_user_authenticated" : is_user_authenticated
# 	}

# 	return render(request, "change_password.html", context)



def login_user(request):
	context = {}

	# if this is a post request we need to process the form data
	if request.method == "POST":

		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()
			
		else:
			print(form)
			# cleaned_data retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password = password)

			if user is not None:
				context = {
					'username' : username,
					'password' : password
				}
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
			
	
	return render(request, "login.html", context)








	# username = 'johndoe'
	# password = 'johndoe1234567'

	# user = authenticate(username = username, password = password)

	

def logout_user(request):
	logout(request)
	return redirect("todolist:index")


def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_task.html", context)


def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id)
	print(todoitem)
	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == "POST":
		form  = UpdateTaskForm (request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:

				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()

				return redirect("todolist:viewtodoitem", todoitem_id =  todoitem[0].id)
			else:

				context = {
					"error" : True
				}

	return render(request, "update_task.html", context)

def delete_task(request, todoitem_id):

	ToDoItem.objects.filter(pk = todoitem_id).delete()

	return redirect("todolist:index")




def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = EventItem.objects.filter(event_name = event_name, user_id = request.user.id)
			

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				EventItem.objects.create(event_name = event_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_event.html", context)



def update_event(request, eventitem_id):

	eventitem = EventItem.objects.filter(pk = eventitem_id)
	print(eventitem)
	context = {
		"user": request.user,
		"eventitem_id": eventitem_id,
		"event_name": eventitem[0].event_name,
		"description": eventitem[0].description,
		"status" : eventitem[0].status
	}

	if request.method == "POST":
		form  = UpdateEventForm (request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if eventitem:

				eventitem[0].event_name = event_name
				eventitem[0].description = description
				eventitem[0].status = status

				eventitem[0].save()

				return redirect("todolist:vieweventitem", eventitem_id =  eventitem[0].id)
			else:

				context = {
					"error" : True
				}

	return render(request, "update_event.html", context)

def delete_event(request, eventitem_id):

	EventItem.objects.filter(pk = eventitem_id).delete()

	return redirect("todolist:index")


def change_password(request):
    user = request.user

    if request.method == 'POST':
        # create form instance and populate it with data from the request
        form = UpdateProfileForm(request.POST)

        if form.is_valid():
            # process form data
            user.first_name = form.cleaned_data['firstname']
            user.last_name = form.cleaned_data['lastname']
            user.set_password(form.cleaned_data['password'])
            user.save()

            # update the user's session to prevent them from being logged out
            update_session_auth_hash(request, user)

        

            # redirect to the same page to avoid form resubmission
            return redirect('todolist:index')

    else:
        # create a blank form instance
        form = UpdateProfileForm()

    context = {
        'form': form,
        'user': user,
    }

    return render(request, 'change_password.html', context)